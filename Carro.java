public class Carro {

    public static String PRETA = "Preta";

    private Integer quantidadePortas;
    private String cor;
    private Integer numeroChassi;
    private Integer anoFabricacao;
    private Integer combustivel;

    public Carro(Integer quantidadePortas, Integer numeroChassi, Integer anoFabricacao, Integer combustivel) {
        this.quantidadePortas = quantidadePortas;
        this.numeroChassi = numeroChassi;
        this.anoFabricacao = anoFabricacao;
        this.combustivel = combustivel;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public void imprimiValores() {
        System.out.println("Quantidade de Portas: " +quantidadePortas);
        System.out.println("Cor do carro: " +getCor());
        System.out.println("Número do Chassi: " +numeroChassi);
        System.out.println("Ano da Fabricação: " +anoFabricacao);
        System.out.println("Combustivel: " +combustivel+ " Litros");
    }
}